

@ui.UILoadable
class hdf5dialog(Qt.QDialog):
    def __init__(self, parent=None):
        Qt.QDialog.__init__(self, parent)
        self.loadUi()  # load the ui file


def hdf5dialog():
    filename =  Qt.QFileDialog.getOpenFileName(None,'Open hdf5 file with rois',filter="hdf5 (*h5)\nall files ( * )"  )
    filename=str(filename)
    if len(filename):
        import PyMca5.PyMcaGui.HDF5Widget as HDF5Widget
        storage=[None]
        def mySlot(ddict):
            name = ddict["name"]
            storage[0]=name
        # browse
        self.__hdf5Dialog = hdf5dialog()
        self.__hdf5Dialog.setWindowTitle('Select a Group  containing roi_definitions by a double click')
        self.__hdf5Dialog.mainLayout =  self.__hdf5Dialog.verticalLayout_2
        fileModel = HDF5Widget.FileModel()
        fileView = HDF5Widget.HDF5Widget(fileModel)
        hdf5File = fileModel.openFile(filename)
        shiftsDataset = None
        fileView.sigHDF5WidgetSignal.connect(mySlot)
        self.__hdf5Dialog.mainLayout.addWidget(fileView)
        self.__hdf5Dialog.resize(400, 200)

        ret = self.__hdf5Dialog.exec_()
        print ret
        hdf5File.close()

        if ret:
            return filename +":" + name
        else:
            return filename +":" + name
    else:
        return filename
            
