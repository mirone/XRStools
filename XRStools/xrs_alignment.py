#!/usr/bin/python
# Filename: xrs_alignment.py

#/*##########################################################################
#
# The XRStools software package for XRS spectroscopy
#
# Copyright (c) 2013-2014 European Synchrotron Radiation Facility
#
# This file is part of the XRStools XRS spectroscopy package developed at
# the ESRF by the DEC and Software group and contains practical functions, 
# most of which are translated from Matlab functions from the University of
# Helsinki Electronic Structure Laboratory.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/
__author__ = "Christoph J. Sahle - ESRF"
__contact__ = "christoph.sahle@esrf.fr"
__license__ = "MIT"
__copyright__ = "European Synchrotron Radiation Facility, Grenoble, France"

import math_functions
import numpy as np
import xrs_scans
from scipy import interpolate, optimize

scan72_motornames = ['vdtx1','vdtx2','vdtx3','vdtx4','vdtx5','vdtx6','vdtx7','vdtx8','vdtx9','vdtx10','vdtx11','vdtx12', \
					 'vutx1','vutx2','vutx3','vutx4','vutx5','vutx6','vutx7','vutx8','vutx9','vutx10','vutx11','vutx12', \
					 'vbtx1','vbtx2','vbtx3','vbtx4','vbtx5','vbtx6','vbtx7','vbtx8','vbtx9','vbtx10','vbtx11','vbtx12', \
					 'hrtx1','hrtx2','hrtx3','hrtx4','hrtx5','hrtx6','hrtx7','hrtx8','hrtx9','hrtx10','hrtx11','hrtx12', \
					 'hltx1','hltx2','hltx3','hltx4','hltx5','hltx6','hltx7','hltx8','hltx9','hltx10','hltx11','hltx12', \
					 'hbtx1','hbtx2','hbtx3','hbtx4','hbtx5','hbtx6','hbtx7','hbtx8','hbtx9','hbtx10','hbtx11','hbtx12']

def optimize_analyzer_focus(path, SPECfname, EDFprefix, EDFname, EDFpostfix, roi_obj, scan_number):
	"""Returns position for all 72 TX motors that optimize the analyzer foci.

	Args:
		path        (str): Absolute path to location of the SPEC-file.
		SPECfname   (str): SPEC-file name.
		EDFprefix   (str): Prefix to where EDF-files are stored.
		EDFname     (str): Base name of the EDF-files.
		EDFpostfix  (str): Post-fix used for the EDF-files.
		roi_obj (roi_obj): ROI object of the xrs_rois class.
		Scan_number (int): Scan number of the 72-motor scan.

	Returns:
		Dictionary with motorname - position pairs.

	"""
	TX_positions = {}

	scan72 = xrs_scans.Scan()
	scan72.load(path, SPECfname, EDFprefix, EDFname, EDFpostfix, scan_number)

	edfmats     = scan72.edfmats
	for ii in range(len(scan72_motornames)):
		motor_scale = scan72.counters[scan72_motornames[ii]]
		try:
			TX_pos      = findAnalyzerFocus(edfmats, motor_scale, roi_obj, ii)
			TX_positions[scan72_motornames[ii]] = TX_pos
		except:
			print ('Fit failed for ROI No. %d.'%ii)

	return TX_positions

def findAnalyzerFocus(edfmats,motor_scale,roi_obj,roiNumber):
	""" **findAnalyzerFocus**

	Returns motor position that optimizes the analyzer focus subject to a 2D Gaussian fit.

	Args: 
	-----
	edfmats (np.array): 3D Numpy array containing the EDF-matrices of a scan.
	motorScale (np.array): Motor positions along the scan (analyzer tx-scan).
	roi_obj (xrs_rois.roi_object): ROI object, defined from the scan, should have some margins around the spots.
	roiNumber (int): Number indicating which ROI to be optimized.

	Returns:
	--------
	optPos (float): Motor position that optimizes the focus of the analyzer in question.

	"""
	xmin = min(roi_obj.x_indices[roiNumber])
	xmax = max(roi_obj.x_indices[roiNumber])
	ymin = min(roi_obj.y_indices[roiNumber])
	ymax = max(roi_obj.y_indices[roiNumber])
	x = np.arange(xmin,xmax)
	y = np.arange(ymin,ymax)
	xx, yy = np.meshgrid(y, x)

	sigma_1 = [] # FWHM along dimension 1
	sigma_2 = [] # FWHM along dimension 2

	# go through all images and fit the 2D Gaussian
	for ii in range(edfmats.shape[0]):
		initial_guess = (np.amax(edfmats[ii,xmin:xmax,ymin:ymax]),(ymin+ymax)/2.,(xmin+xmax)/2.,1.0,1.0,0)
		popt, pcov = optimize.curve_fit(math_functions.flat2DGaussian, (xx, yy), edfmats[ii,xmin:xmax,ymin:ymax].ravel(), p0=initial_guess)
		sigma_1.append(popt[3])
		sigma_2.append(popt[4])

	# find the best compromise/minimum for sigma_1 and sigma_2
	x = np.array(motor_scale)
	y1 = np.array(sigma_1)
	y2 = np.array(sigma_2)

	# popt will have (x,amp,x0,fwhm)
	popt_1, pcov_1 = optimize.curve_fit( gauss_forcurvefit, x, y1 )
	popt_2, pcov_2 = optimize.curve_fit( gauss_forcurvefit, x, y2 )

	return np.mean([popt_1[2], popt_2[2]])










