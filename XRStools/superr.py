import numpy as np
import h5py
import math
myrank=0
try:
    import skimage.restoration
except:
    print " ATTENTION : SKIMAGE RESORATION NOT LOADED "

def superr( scalDD, scalDS, scalSS, niter=15, beta=1.0e-8):
    """ 
    -    scalDS  which is  an array  [ZDIM,YDIM,XDIM]  , type "d" .
    -    scalDD  which is the total sum of the squared datas.
    -    scalSS  which is an array [XDIM,XDIM]  , type "d" .
    """
    ZDIM,YDIM,XDIM = scalDS.shape
    Volume = np.zeros( [ZDIM,YDIM,XDIM]  ,"f" )
    assert( scalSS.shape == (XDIM,XDIM))


    scalSS = scalSS.astype("f")
    scalDS = scalDS.astype("f")


    print "  SHAPES ", scalDS.shape, scalSS.shape 

    
    Fista (  scalDD, scalDS, scalSS, Volume,niter=niter, beta=beta)
    
    return Volume

def calculate_grad( scalDD, scalDS , scalSS,   solution, grad) :
    grad [:]  = np.tensordot(  solution, scalSS, axes=[[-1],[-1]])
    err  = (grad*solution).sum()
    if scalDS is not None:
        err -= (scalDS*solution).sum()*2
        err +=  scalDD
        grad [:] -= scalDS
    return err/2
    
def    Fista( scalDD, scalDS , scalSS,  solution      , niter=500, beta=0.1 ):


    grad   = np.zeros_like(solution)
    grad2  = np.zeros_like(solution)
    x_old  = np.zeros_like(solution)
    y      = np.zeros_like(solution)

    err = 0.0
    err=calculate_grad( scalDD, scalDS , scalSS,   solution, grad) 
    for i in range(20):
        
        calculate_grad(None, None , scalSS,   grad, grad2)
        Lip = math.sqrt( np.linalg.norm(grad2/100000.0)   )*100000
        grad[:]  = grad2/ Lip
        if myrank==0:
            print "LIP ", Lip
    Lip = Lip*1.2

    t=1.0
    y[:] = solution
    x_old[:] = solution
    for iter in range(abs(niter)):
        err = calculate_grad(scalDD, scalDS , scalSS,   y, grad) 

        solution[:] =  y - grad/Lip
        solution[:]=skimage.restoration.denoise_tv_chambolle(solution, weight=beta, eps=0.000002) 


        ## solution[:] = np.maximum(solution, 0)

        tnew = ( 1+math.sqrt(1.0+4*t*t) )/2


        y[:] = solution +(t-1)/tnew *( solution - x_old )
        t = tnew
        if niter<0:
            t=1
        x_old[:] = solution
        if myrank==0:
            print " errore est %e  mod_grad est  %e\n" % (  err, grad.std()) 



