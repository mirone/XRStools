#!/usr/bin/python
# Filename: xrs_scans.py

#/*##########################################################################
#
# The XRStools software package for XRS spectroscopy
#
# Copyright (c) 2013-2014 European Synchrotron Radiation Facility
#
# This file is part of the XRStools XRS spectroscopy package developed at
# the ESRF by the DEC and Software group.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/
__author__ = "Christoph J. Sahle - ESRF"
__contact__ = "christoph.sahle@esrf.fr"
__license__ = "MIT"
__copyright__ = "European Synchrotron Radiation Facility, Grenoble, France"

import numpy as np
import xrs_utilities, math_functions, xrs_fileIO, xrs_rois
import h5py
import os

from itertools import groupby
from scipy import optimize
from matplotlib import pylab as plt

# try to import the fast PyMCA parsers
try:
    import PyMca5.PyMcaIO.EdfFile as EdfIO
    import PyMca5.PyMcaIO.specfilewrapper as SpecIO
    use_PyMca = True
except:
    use_PyMca = False

print( ' >>>>>>>>  use_PyMca ' , use_PyMca)

__metaclass__ = type # new style classes

class Scan:
	"""New version of a container holding information of a single scan. 

	All relevant information from the SPEC- and EDF-files are organized
	in instances of this class.

	Attributes:
		edf_mats      (np.array): Array containing all 2D images that belong to the scan.
		number             (int): Scan number as in the SPEC file.
 		scan_type       (string): Keyword, used later to group scans (add similar scans, etc.).
		energy        (np.array): Array containing the energy axis (1D).
		monitor       (np.array): Array containing the monitor signal (1D).
		counters    (dictionary): Counters with assiciated data from the SPEC file.
		motors            (list): Motor positions as found in the SPEC file header.
		eloss         (np.array): Array of the energy loss scale.
		signals       (np.array): Array of signals extracted from the ROIs.
		errors        (np.array): Array of Poisson errors.
		cenom             (list): Center of mass for each ROI (used if scan is an elastic line scan).
		signals_pw        (list): Pixelwise (PW) data, one array of PW data per ROI.
		errors_pw         (list): Pixelwise (PW) Poisson errors, one array of PW errors per ROI.
		cenom_pw          (list): Center of mass for each pixel.
		signals_pw_interp (list): Interpolated signals for each pixel.

	"""

	def __init__(self):
		self.edfmats    = np.array([])
		self.scan_number= None
		self.scan_type  = None
		self.energy     = np.array([])
		self.monitor    = np.array([])
		self.counters   = {}
		self.motors     = []
		self.eloss      = []
		self.signals    = []
		self.errors     = []
		self.cenom      = []
		self.signals_pw = []
		self.errors_pw  = []
		self.cenom_pw   = []
		self.signals_pw_interp = []

	def load(self, path, SPECfname, EDFprefix, EDFname, EDFpostfix, scan_number, \
				direct=False, roi_obj=None, scaling=None, scan_type='generic', \
				en_column=None, moni_column='izero'):
		"""Parse SPEC-file and EDF-files for loading a scan.

		Note:
			If 'direct' is 'True' all EDF-files will be deleted 
			after application of the ROIs.

		Args:
			path        (str): Absolute path to directory in which the SPEC-file is located.
			SPECfname   (str): SPEC-file name.
			EDFprefix   (str): Prefix for the EDF-files.
			EDFpostfix  (str): Postfix for the EDF-files.
			scan_number (int): Scan number of the scan to be loaded.
			direct  (boolean): If 'True', all EDF-files will be deleted after loading the scan.

		"""

		print( 'Parsing EDF- and SPEC-files of scan No. %s.' % scan_number)

		self.scan_number = scan_number

		# load SPEC-file
		fname = os.path.join(path , SPECfname)
		if use_PyMca == True:
			spec_data, self.motors, self.counters = xrs_fileIO.PyMcaSpecRead(fname,scan_number)
		else:
			spec_data, self.motors, self.counters = xrs_fileIO.SpecRead(fname,scan_number)

		# assign values, energy only if en_column is specified, first counter in SPECfile otherwise
		if en_column:
			self.energy    = np.array(self.counters[en_column.lower()])
		else:
			self.energy    = np.array(self.counters[self.counters.keys()[0].lower()])
		self.monitor   = np.array(self.counters[moni_column.lower()])
		self.scan_type = scan_type

		# load EDF-files
		self.edfmats = xrs_fileIO.ReadEdfImages_my( self.counters['ccdno'], path, EDFprefix, EDFname, EDFpostfix )

		# apply ROIs (if applicable)
		if direct and isinstance(roi_obj, xrs_rois.roi_object):
			self.apply_rois(roi_obj, scaling=scaling)
			print('Deleting EDF-files of scan No. %s.' % scan_number)
			self.edfmats = np.array([])

	def assign(self, edf_arrays, scan_number, energy_scale, monitor_signal, counters, \
				motor_positions,specfile_data,scan_type='generic'):
		"""Method to group together existing data from a scan (for backward compatibility).

		Args:
			edf_arrays     (np.array): Array of all 2D images that belong to the scan.
			scan_number         (int): Number under which this scan can be found in the SPEC file. 
			energy_scale   (np.array): Array of the energy axis.
			monitor_signal (np.array): Array of the monitor signal.
			counters     (dictionary): Counters with assiciated data from the SPEC file.
			motor_positions    (list): Motor positions as found in the SPEC file header.
			specfile_data  (np.array): Matrix with all data as found in the SPEC file.
			scantype            (str): Keyword, used later to group scans (add similar scans, etc.).

		"""
		self.edfmats    = np.array(edf_arrays)
		self.scan_number= scan_number
		self.energy     = np.array(energy_scale)
		self.monitor    = np.array(monitor_signal)
		self.counters   = counters
		self.motors     = motor_positions
		spec_data  = specfile_data
		self.scan_type  = scan_type

	def save_hdf5(self,fname):
		"""Save a scan in an HDF5 file.

		Note:
			HDF5 files are strange for overwriting files.
			DOES NOT SAVE COUNTERS or MOTORS YET... NEED A DEEPER H5!

		Args:
			fname (str): Path and filename for the HDF5 file.

		"""

		f = h5py.File(fname, "w")
		for attr in ['edfmats', 'scan_number', 'energy', 'monitor', 'scan_type']:
			print attr
			f[attr] = eval( 'self.' + attr )

		f.close()

	def load_hdf5(self,fname):
		"""Load a scan from an HDF5 file.

		Args:
			fname (str): Filename of the HDF5 file.
		"""

		f = h5py.File(fname, "r")
		#for attr in ['edfmats', 'scan_number', 'energy', 'monitor', 'counters', 'motors', 'scan_type']:
		#	eval( 'self.' + attr ) = f[attr]
		#f.close()


	def apply_rois(self,roi_obj,scaling=None):
		"""Sums up intensities in each ROI.

		Args:
			roi_obj (instance): Instance of the 'XRStools.xrs_rois.roi_object' class defining the ROIs.
			scaling (np.array): Array of float-type scaling factors (factor for each ROI).

		Returns:
			None if there are not EDF-files to apply the ROIs to.
		"""

		# make sure there is data loaded
		if self.edfmats.size == 0:
			print( 'Please load some EDF-files first.' )
			return

		# apply ROIs
		signals = np.zeros((len(self.energy), len(roi_obj.red_rois)))
		for ii in range(len(self.edfmats)):
			ind = 0
			for key, (pos, M) in roi_obj.red_rois.iteritems():
				S     = M.shape
				inset = (slice( pos[0], pos[0]+(S[0]+1) ), slice( pos[1], pos[1]+(S[1]+1) ))
				signals[ii,ind] = np.sum( self.edfmats[ii, inset[0], inset[1]])
				ind += 1

		# assign
		self.signals = signals
		self.errors  = np.sqrt(signals)

		# apply scaling (if applicable)
		if np.any(scaling):
			# make sure, there is one scaling factor for each ROI
			assert len(scaling) == signals.shape[1]
			for ii in range(signals.shape[1]):
				self.signals[:,ii] *= scaling[ii]
				self.errors[:,ii]  *= scaling[ii]

	def apply_rois_pw(self,roi_obj,scaling=None):
		"""Pixel-wise reading of the ROIs' pixels into a list of arrays. 

		I.e. each n-pixel ROI will have n Spectra, saved in a 2D array.

		Args:
		roi_obj (instance): Instance of the 'XRStools.xrs_rois.roi_object' class defining the ROIs.
		scaling (list) or (np.array): Array or list of float-type scaling factors (one factor for each ROI).

		"""
		data   = [] # list of 2D arrays (energy vs. intensity for each pixel inside a single ROI) 
		errors = []
		for n in range(len(roi_obj.indices)): # each ROI
			roidata = np.zeros((len(self.edfmats),len(roi_obj.indices[n]))) # 2D np array energy vs pixels in current roi
			for m in range(len(self.edfmats)): # each energy point along the scan
				for l in range(len(roi_obj.indices[n])): # each pixel on the detector
					roidata[m,l] = self.edfmats[m,roi_obj.indices[n][l][0],roi_obj.indices[n][l][1]]
			data.append(roidata) # list which contains one array (energy point, pixel) per ROI
			errors.append(np.sqrt(roidata))

		# using red_rois, not sure if the order of the pixels is preserved
		#for key, (pos, M) in roi_obj.red_rois.iteritems():
		#	roidata = np.zeros((len(self.edfmats),prod(roi_obj.red_rois[key][1].shape)))
		#	S       = M.shape
		#	inset = (slice( pos[0], pos[0]+(S[0]+1) ), slice( pos[1], pos[1]+(S[1]+1) ))
		#	for ii in range(len(self.edfmats)):
		#		roidata[ii,:] = self.edfmats[ii, inset[0], inset[1]].ravel()
		#	data.append(roidata)

		self.signals_pw = data
		self.errors_pw  = errors

		# apply scaling (if applicable)
		if np.any(scaling):
			# make sure, there is one scaling factor for each ROI
			assert len(scaling) == len(roi_obj.indices)
			for ii in range(len(roi_obj.indices)):
				self.signals_pw[ii] *= scaling[ii]
				self.errors[ii]  *= scaling[ii]

	def get_type(self):
		"""Returns the type of the scan.
		"""
		return self.scan_type

	def get_scan_number(self):
		"""Returns the number of the scan.
		"""
		return self.scan_number

	def get_shape(self):
		"""Returns the shape of the matrix holding the signals.
		"""
		if not np.any(self.signals):
			print 'please apply the ROIs first.'
			return
		else:
			return self.signals.shape

	def get_numofrois(self):
		"""Returns the number of ROIs applied to the scan.
		"""
		if not self.signals.any():
			print 'please apply the ROIs first.'
			return
		else:
			return self.signals.shape[1]


class scan:
	"""
	Container class, holding information of single scans performed with 2D detectors. 
	"""
	def __init__(self,edf_arrays,scannumber,energy_scale,monitor_signal,counters,motor_positions,specfile_data,scantype='generic'):
		# rawdata
		self.edfmats  = np.array(edf_arrays)          # numpy array of all 2D images that belong to the scan
		self.number   = scannumber                    # number under which this scan can be found in the SPEC file 
		self.scantype = scantype                      # keyword, later used to group scans (add similar scans, etc.)
		self.energy   = np.array(energy_scale)        # numpy array of the energy axis
		self.monitor  = np.array(monitor_signal)      # numpy array of the monitor signal
		# some things maybe not imediately necessary 
		self.counters = counters                      # names of all counters that appear in the SPEC file for this scan (maybe unnecessary)
		self.motors   = motor_positions               # all motor positions as found in the SPEC file header for this scan ( " )
		self.specdata = np.array(specfile_data)       # all data that is also in the SPEC file for this scan
		# data (to be filled after defining rois)
		self.eloss    = []     # numpy array of the energy loss scale
		self.signals  = []     # numpy array of signals extracted from the ROIs
		self.errors   = []     # numpy array of all Poisson errors
		self.cenom    = []     # list with center of masses (used if scan is an elastic line scan)
		# pixel-wise things
		self.signals_pw = []   
		self.cenom_pw   = []
		self.signals_pw_interp = []

	def applyrois(self,indices,scaling=None):
		"""
		Sums up intensities found in the ROIs of each detector image
		and stores it into the self.signals attribute.
		roi_object = instance of the 'rois' class redining the ROIs
		scaling    = numpy array of numerical scaling factors (has to be one for each ROIs)
		"""
		data = np.zeros((len(self.edfmats),len(indices)))
		for n in range(len(indices)): # each roi
			for m in range(len(self.edfmats)): # each energy point along the scan
				for l in range(len(indices[n])): # each pixel on the detector
					data[m,n] += self.edfmats[m,indices[n][l][0],indices[n][l][1]]
		self.signals = np.array(data)
		self.errors  = np.sqrt(data)
		if np.any(scaling):
			assert len(scaling) == len(indices) # make sure, there is one scaling factor for each roi
			for ii in range(len(indices)):
				self.signals[:,ii] *= scaling[ii]
				self.errors[:,ii]  *= scaling[ii]

	def applyrois_pw(self,indices,scaling=None):
		"""
		Pixel-wise reading of the ROI's pixels into a list of arrays. I.e. each n-pixel ROI will have n Spectra, saved in a 2D array.
		Parameters
		----------
		indices : list
			List of indices (attribute of the xrs_rois class).
		scaling : list of flaots, optional
			Python list of scaling factors (one per ROI defined) to be applied to all pixels of that ROI.

		"""
		data = [] # list of 2D arrays (energy vs. intensity for each pixel inside a single ROI) 
		for n in range(len(indices)): # each ROI
			roidata = np.zeros((len(self.edfmats),len(indices[n]))) # 2D np array energy vs pixels in current roi
			for m in range(len(self.edfmats)): # each energy point along the scan
				for l in range(len(indices[n])): # each pixel on the detector
					roidata[m,l] = self.edfmats[m,indices[n][l][0],indices[n][l][1]]
			data.append(roidata) # list which contains one array (energy point, pixel) per ROI
		self.signals_pw = data

	def get_eloss_pw(self):
		"""
		Finds the center of mass for each pixel in each ROI, sets the energy loss scale in 
		and interpolates the signals to a common energy loss scale. Finds the resolution (FWHM) for
		each pixel.

		"""
		if not self.scantype == 'elastic': # return, if scantype is not elastic
			print 'This method is meant for elastic line scans only!'
			return
		if not self.signals_pw: # return, if there is no data
			print 'Please use the applyrois_pw function first!'
			return
		else:
			cenom_pw      = []
			resolution_pw = []
			for roiind in range(len(self.signals_pw)): # each ROI
				oneroi_cenom      = []
				oneroi_resolution = []
				for pixelind in range(len(self.signals_pw[roiind])): # each pixel in the ROI
					oneroi_cenom.append(xrs_utilities.find_center_of_mass(self.energy,self.signals_pw[roiind][:,pixelind]))
					try: 
						FWHM,x0 = xrs_utilities.fwhm((self.energy - oneroi_cenom[roiind][pixelind])*1e3,self.signals_pw[roiind][:,pixelind])
						oneroi_resolution.append(FWHM)
					except:
						oneroi_resolution.append(0.0)
				cenom_pw.append(oneroi_cenom)
				resolution_pw.append(oneroi_resolution)
			# define mean of first ROI as 'master' energy loss scale for all Pixels
			self.eloss = (self.energy - np.mean(cenom_pw[0]))*1e3 # average energy loss in eV from first ROI
			# !!!! mayb it is better to just save the CENOM values and to the interpolation later with the stiched 
			# !!!! whole spectrum... 
			# define eloss-scale for each ROI and interpolate onto the 'master' eloss-scale
			#for roiind in range(len(self.signals_pw)): # each ROI
			#	oneroi_signals = np.zeros_like(self.signals_pw[roiind])
			#	for pixelind in range(len(self.signals_pw[roiind])):
			#		x = (self.energy-self.cenom_pw[roiind][pixelind])*1e3
			#		y =
			#		f = interp1d(x,y, bounds_error=False,fill_value=0.0)
			#		self.signals[:,n] = f(self.eloss)
			#self.signals_pw_interp = []

	def get_type(self):
		return self.scantype

	def get_scannumber(self):
		return self.number

	def get_shape(self):
		if not np.any(self.signals):
			print 'please apply the ROIs first.'
			return
		else:
			return np.shape(self.signals)

	def get_numofrois(self):
		if not self.signals.any():
			print 'please apply the ROIs first.'
			return
		else:
			return np.shape(self.signals)[1]

class scangroup:
	"""
	Container class holding information from a group of scans.
	"""
	def __init__(self,energy,signals,errors,grouptype='generic'):
		self.energy    = energy     # common energy scale
		self.eloss     = []         # common energy loss scale
		self.signals   = signals    # summed up signals
		self.errors    = errors     # Poisson errors
		self.grouptype = grouptype  # keyword common to all scans
		self.signals_orig = signals # keep a copy of uninterpolated data

	def get_type(self):
		return self.grouptype

	def get_meanenergy(self):
		return np.mean(self.energy)

	def get_estart(self):
		return self.energy[0]

	def get_eend(self):
		return self.energy[-1]

	def get_meanegridspacing(self):
		return np.mean(np.diff(self.energy))

	def get_maxediff(self):
		return (self.energy[-1]-self.energy[0])

class offDiaDataSet:
	""" **offDiaDataSet**
	Class to hold information from an off-diagonal dataset.
	"""
	def __init__(self):
		self.RCmonitor    = np.array([])
		self.signalMatrix = np.array([])
		self.errorMatrix  = np.array([])
		self.motorMatrix  = np.array([])
		self.I0Matrix     = np.array([])
		self.energy       = np.array([])
		self.eloss        = np.array([])
		self.ROI_number   = 0
		self.G_vector     = np.array([])
		self.q0           = np.array([])
		self.qh           = np.array([])
		self.k0           = np.array([])
		self.kh           = np.array([])
		self.kprime       = np.array([])
		self.alignedSignalMatrix = np.array([])
		self.alignedErrorMatrix  = np.array([])
		self.alignedRCmonitor    = np.array([])
		self.masterRCmotor= np.array([])

	def filterDetErrors(self,threshold=3000000):
		inds = np.where(self.signalMatrix >= threshold)
		for ii in range(len(inds[0])):
			self.signalMatrix[inds[0][ii], inds[1][ii]] = 0.0
			self.signalMatrix[inds[0][ii], inds[1][ii]] = np.interp(inds[0][ii], [inds[0][ii]-1,inds[0][ii]+1] , [self.signalMatrix[inds[0][ii], inds[1][ii]-1],self.signalMatrix[inds[0][ii], inds[1][ii]+1]])

	def normalizeSignals(self):
		self.signalMatrix /= self.I0Matrix
		self.errorMatrix  /= self.I0Matrix
		self.RCmonitor    /= self.I0Matrix
		self.signalMatrix *= np.mean(self.I0Matrix)
		self.errorMatrix  *= np.mean(self.I0Matrix)
		self.RCmonitor    *= np.mean(self.I0Matrix)

	def alignRCmonitor(self):
		# check if data exists
		if not np.any(self.RCmonitor):
			print('Please load some data first.')
			return
		#x = np.arange(len(self.motorMatrix.T[:,0]))

		#RCmonitor   = self.RCmonitor#[:,diagonal_inds[0]:-diagonal_inds[1]]
		#motorMatrix = self.motorMatrix#[:,diagonal_inds[0]:-diagonal_inds[1]]
		#signalMatrix= self.signalMatrix#[:,diagonal_inds[0]:-diagonal_inds[1]]

		RCposition = []
		for ii in range(len(self.RCmonitor)):
			x = self.motorMatrix[ii,:]
			y = self.RCmonitor[ii,:]
			try:
				guess = [x[np.where(y == np.amax(y))[0]][0], 0.01, 1.0, np.amax(y), 1.]
				popt, pcov = optimize.curve_fit(math_functions.pearson7_forcurvefit, x, y,p0=guess)	
				RCposition.append(popt[0])
			except: 
				RCposition.append(xrs_utilities.find_center_of_mass(x,y))

		# possibly correct for deviations from Bragg's law
		#RCfit = np.polyval(np.polyfit(self.energy,self.RCposition),self.energy)
		#for ii in range(len(RCfit)):

		master_phi = self.motorMatrix[10,:]-RCposition[10]
		signalMatrix = np.zeros((len(self.energy),len(master_phi)))
		errorMatrix  = np.zeros((len(self.energy),len(master_phi)))
		RCmonitor    = np.zeros((len(self.energy),len(master_phi)))
		for ii in range(len(self.energy)):
			signalMatrix[ii,:] = np.interp(master_phi,self.motorMatrix[ii,:]-RCposition[ii],self.signalMatrix[ii,:])
			errorMatrix[ii,:]  = np.interp(master_phi,self.motorMatrix[ii,:]-RCposition[ii],self.errorMatrix[ii,:])
			RCmonitor[ii,:]    = np.interp(master_phi,self.motorMatrix[ii,:]-RCposition[ii],self.RCmonitor[ii,:])

		self.alignedSignalMatrix = signalMatrix
		self.alignedErrorMatrix  = errorMatrix
		self.masterRCmotor       = master_phi
		self.alignedRCmonitor    = RCmonitor

	def alignRCmonitorCC(self,repeat=2):
		""" **alignRCmonitorCC**
		Use cross-correlation to align data matrix according to the Rockin-Curve monitor.
		"""
		# check if data exists
		if not np.any(self.RCmonitor):
			print('Please load some data first.')
			return

		signalMatrix = np.zeros((len(self.energy),len(self.motorMatrix[0,:])))
		errorMatrix  = np.zeros((len(self.energy),len(self.motorMatrix[0,:])))
		RCmonitor    = np.zeros((len(self.energy),len(self.motorMatrix[0,:])))

		# first iteration
		for ii in range(len(self.RCmonitor)):
			x0 = self.RCmonitor[0,:]
			x  = self.RCmonitor[ii,:]
			y  = np.correlate(x0,x,mode='same')
			ind = np.where(y == np.amax(y))[0]
			if ii == 0:
				master_phi = self.motorMatrix[ii,:] - self.motorMatrix[ii,ind]

			signalMatrix[ii,:] = np.interp(master_phi,self.motorMatrix[ii,:]-self.motorMatrix[ii,ind],self.signalMatrix[ii,:])
			errorMatrix[ii,:]  = np.interp(master_phi,self.motorMatrix[ii,:]-self.motorMatrix[ii,ind],self.errorMatrix[ii,:])
			RCmonitor[ii,:]    = np.interp(master_phi,self.motorMatrix[ii,:]-self.motorMatrix[ii,ind],self.RCmonitor[ii,:])

		# further iterations
		if repeat:
			for jj in range(repeat):
				for ii in range(len(RCmonitor)):
					x0 = RCmonitor[0,:]
					x  = RCmonitor[ii,:]
					y  = np.correlate(x0,x,mode='same')
					ind = np.where(y == np.amax(y))[0]
					signalMatrix[ii,:] = np.interp(master_phi,master_phi-master_phi[ind],self.signalMatrix[ii,:])
					errorMatrix[ii,:]  = np.interp(master_phi,master_phi-master_phi[ind],self.errorMatrix[ii,:])
					RCmonitor[ii,:]    = np.interp(master_phi,master_phi-master_phi[ind],self.RCmonitor[ii,:])

		self.alignedSignalMatrix = signalMatrix
		self.alignedErrorMatrix  = errorMatrix
		self.masterRCmotor       = master_phi
		self.alignedRCmonitor    = RCmonitor

	def deglitchSignalMatrix(self,startpoint,stoppoint,threshold):
		signalMatrix = self.alignedSignalMatrix
		for ii in range(signalMatrix.shape[1]):
			ind = np.where(signalMatrix[startpoint:stoppoint,ii] >= threshold)[0]
			if np.any(ind):
				signalMatrix[startpoint+ind, ii] = (signalMatrix[startpoint+ind-1,ii] + signalMatrix[startpoint+ind+1,ii] )/2.0
		self.alignedSignalMatrix = signalMatrix

	def interpolateMatrix(self,master_matrix,master_energy,master_RCmotor):
		from scipy import interpolate
		signalMatrix = self.alignedSignalMatrix
		y=self.energy
		x=self.masterRCmotor
		xx, yy = np.meshgrid(x, y)
		z = signalMatrix
		f = interpolate.interp2d(x, y, z, kind='cubic')
		ynew = master_energy
		xnew = master_RCmotor
		znew = f(xnew, ynew)
		self.interpSignalMatrix = znew
		self.interpEnergy  = ynew
		self.interpRCmotor = xnew

	def removeElastic(self,fitrange=[-6.0,2.0]):
		self.energy = np.array(self.energy)
		cenom = []
		for ii in range(self.signalMatrix.shape[1]):
			inds = np.where(np.logical_and(np.array(self.energy) >= fitrange[0], np.array(self.energy)<= fitrange[1]))[0]
			x = self.energy[inds]
			y = self.alignedSignalMatrix[inds,ii]
			guess = [x[np.where(y==np.amax(y))[0]], 0.002, 100.0,  np.amax(y) ,0.0]
			fitfct = lambda a: np.sum( (y - math_functions.pearson7(x,a) )**2.0 )
			params = optimize.minimize(fitfct, guess, method='SLSQP').x
			cenom.append(params[0])
			back = math_functions.pearson7(self.energy,params )
			plt.ion()
			plt.cla()
			plt.plot(self.energy,self.alignedSignalMatrix[:,ii],self.energy,back,self.energy,self.alignedSignalMatrix[:,ii]-back)
			plt.waitforbuttonpress()
			self.alignedSignalMatrix[:,ii] = self.alignedSignalMatrix[:,ii]-back
		self.eloss = (self.energy - np.mean(cenom))*1.0e3

	def removeElastic2(self, fitrange1, fitrange2, guess=None):
		""" **removeElastic2**
		Subtract Pearson7 plus linear.
		"""
		self.alignedSignalMatrixB = np.zeros_like(self.alignedSignalMatrix)
		self.energy = np.array(self.energy)
		cenom = []
		for ii in range(self.signalMatrix.shape[1]):
			region1 = np.where(np.logical_and(np.array(self.energy) >= fitrange1[0], np.array(self.energy) <= fitrange1[1]))[0]
			region2 = np.where(np.logical_and(np.array(self.energy) >= fitrange2[0], np.array(self.energy) <= fitrange2[1]))[0]
			inds    = np.append(region1,region2)
			x = self.energy[inds]
			y = self.alignedSignalMatrix[inds,ii]
			if not guess:
				guess = [x[np.where(y==np.amax(y))[0]], 0.001, 1.0,  np.amax(y) ,0.0, -0.1, 0.01]
			fitfct = lambda a: np.sum( (y - math_functions.pearson7_linear(x,a) )**2.0 )
			params = optimize.minimize(fitfct, guess, method='COBYLA',tol=1e-20).x
			cenom.append(params[0])
			back = math_functions.pearson7_linear(self.energy,params )
			plt.ion()
			plt.cla()
			plt.plot(self.energy,self.alignedSignalMatrix[:,ii],self.energy,back,self.energy,self.alignedSignalMatrix[:,ii]-back)
			plt.waitforbuttonpress()
			self.alignedSignalMatrixB[:,ii] = self.alignedSignalMatrix[:,ii]-back
		self.eloss = (self.energy - np.mean(cenom))*1.0e3

	def windowSignalMatrix(self,estart,estop):
		inds = np.where(np.logical_and(self.eloss>=estart, self.eloss<= estop))[0]
		self.alignedSignalMatrix = self.alignedSignalMatrix[inds[0]:inds[-1],:]

	def removeLinearBack(self,fitrange1,fitrange2):
		self.energy = np.array(self.energy)
		for ii in range(self.signalMatrix.shape[1]):
			region1 = np.where(np.logical_and(np.array(self.energy) >= fitrange1[0], np.array(self.energy) <= fitrange1[1]))
			region2 = np.where(np.logical_and(np.array(self.energy) >= fitrange2[0], np.array(self.energy) <= fitrange2[1]))
			region  = np.append(region1,region2)
			x = np.array(self.energy[region])
			y = self.alignedSignalMatrix[region,ii]
			back = np.polyval(np.polyfit(x,y,1),self.energy)
			self.alignedSignalMatrix[:,ii] -= back

	def removeConstBack(self,fitrange1,fitrange2):
		self.energy = np.array(self.energy)
		for ii in range(self.signalMatrix.shape[1]):
			region1 = np.where(np.logical_and(np.array(self.energy) >= fitrange1[0], np.array(self.energy) <= fitrange1[1]))
			region2 = np.where(np.logical_and(np.array(self.energy) >= fitrange2[0], np.array(self.energy) <= fitrange2[1]))
			region  = np.append(region1,region2)
			x = np.array(self.energy[region])
			y = self.alignedSignalMatrix[region,ii]
			back = np.polyval(np.polyfit(x,y,0),self.energy)
			self.alignedSignalMatrix[:,ii] -= back

	def removePearsonBack(self,fitrange1,fitrange2):
		self.energy = np.array(self.energy)
		for ii in range(self.signalMatrix.shape[1]):
			region1 = np.where(np.logical_and(np.array(self.energy) >= fitrange1[0], np.array(self.energy) <= fitrange1[1]))
			region2 = np.where(np.logical_and(np.array(self.energy) >= fitrange2[0], np.array(self.energy) <= fitrange2[1]))
			region  = np.append(region1,region2)
			x = np.array(self.energy[region])
			y = self.alignedSignalMatrix[region,ii]
			guess = [x[np.where(y==np.amax(y))[0]], 0.002, 100.0,  np.amax(y) ,0.0]
			fitfct = lambda a: np.sum( (y - math_functions.pearson7(x,a) )**2.0 )
			params = optimize.minimize(fitfct, guess, method='SLSQP').x
			back = math_functions.pearson7(self.energy,params)
			self.alignedSignalMatrix[:,ii] -= back

	def replaceSignalByConstant(self,fitrange):
		self.energy = np.array(self.energy)
		for ii in range(self.signalMatrix.shape[1]):
			inds = np.where(np.logical_and(np.array(self.energy) >= fitrange[0], np.array(self.energy) <= fitrange[1]))[0]
			x = np.array(self.energy[inds])
			y = self.alignedSignalMatrix[inds,ii]
			back = np.polyval(np.polyfit(x,y,0),self.energy)
			self.alignedSignalMatrix[:,ii] = back

def findgroups(scans):
	"""
	this groups together instances of the scan class based on their  "scantype" attribute and returns ordered scans
	"""	
	allscannames = []
	for scan in scans:
		print scan
		allscannames.append(scan)
	allscannames.sort() # 
	allscans = []
	for scan in allscannames:
		 allscans.append(scans[scan])
	allscans = sorted(allscans,key=lambda x:x.get_type())
	rawgroups = []
	results = groupby(allscans,lambda x:x.get_type())
	print 'The following scangroups were found:'
	for key,thegroup in results:
		print key
		rawgroups.append(list(thegroup))
	return rawgroups

def makegroup(groupofscans,grouptype=None):
	"""
	takes a group of scans, sums up the signals and monitors, estimates poisson errors, and returns an instance of the scangroup 		class (turns several instances of the "scan" class into an instance of the "scangroup" class)
	"""
	if not grouptype:
		grouptype = groupofscans[0].get_type() # the type of the sum of scans will be the same as the first from the list	
	theenergy   = groupofscans[0].energy # all scans are splined onto energy grid of the first scan
	thesignals  = np.zeros(groupofscans[0].get_shape())
	theerrors   = np.zeros(groupofscans[0].get_shape())
	themonitors = np.zeros(np.shape(groupofscans[0].monitor))
	for scan in groupofscans:
		f  = interpolate.interp1d(scan.energy,scan.monitor, bounds_error=False, fill_value=0.0)
		moni = f(theenergy)
		themonitors += moni
		for n in range(thesignals.shape[-1]):
			f = interpolate.interp1d(scan.energy,scan.signals[:,n], bounds_error=False, fill_value=0.0)
			signal = f(theenergy)
			thesignals[:,n] += signal
	for n in range(thesignals.shape[-1]):
		theerrors[:,n]  = np.sqrt(thesignals[:,n])
	# and normalize	
	for n in range(thesignals.shape[-1]):
		thesignals[:,n] = thesignals[:,n]/themonitors
		theerrors[:,n]  = theerrors[:,n]/themonitors

	group = scangroup(theenergy,thesignals,theerrors,grouptype)
	return group

def makegroup_nointerp(groupofscans,grouptype=None,absCounts=False,time_counter='seconds'):
	"""
	takes a group of scans, sums up the signals and monitors, estimates poisson errors, and returns an instance of the scangroup class (turns several instances of the "scan" class into an instance of the "scangroup" class), same as makegroup but withouth interpolation to account for encoder differences... may need to add some "linspace" function in case the energy scale is not monotoneous... 
	"""
	if not grouptype:
		grouptype = groupofscans[0].get_type() # the type of the sum of scans will be the same as the first from the list	
	theenergy   = groupofscans[0].energy
	thesignals  = np.zeros(groupofscans[0].get_shape())
	theerrors   = np.zeros(groupofscans[0].get_shape())
	themonitors = np.zeros(np.shape(groupofscans[0].monitor))

	for scan in groupofscans:
		themonitors += scan.monitor*np.array(scan.counters['seconds'])/(np.mean(scan.monitor))
		for n in range(thesignals.shape[-1]):
			thesignals[:,n] += scan.signals[:,n]
		for n in range(thesignals.shape[-1]):
			theerrors[:,n]  = np.sqrt(thesignals[:,n])
	# and normalize
	if not absCounts:
		for n in range(thesignals.shape[-1]):
			thesignals[:,n] = thesignals[:,n]/themonitors
			theerrors[:,n]  = theerrors[:,n]/themonitors
		group = scangroup(theenergy,thesignals,theerrors,grouptype)
	if absCounts:
		for n in range(thesignals.shape[-1]):
			thesignals[:,n] = thesignals[:,n]/themonitors*np.mean(themonitors)
			theerrors[:,n]  = theerrors[:,n]/themonitors*np.mean(themonitors)
		group = scangroup(theenergy,thesignals,theerrors,grouptype)
	return group

def append2Scan_right(group1,group2,inds=None,grouptype='spectrum'):
	"""
	append two instancees of the scangroup class, return instance of scangroup
	append group2[inds] to the right (higher energies) of group1
	if inds is not None, only append rows indicated by inds to the first group 
	"""
	assert isinstance(group1,scangroup) and isinstance(group2,scangroup)
	energy  = group1.energy
	signals = group1.signals
	errors  = group1.errors
	gtype   = group1.grouptype
	if not inds:
		energy  = np.append(energy,group2.energy)
		signals = np.append(signals,np.squeeze(group2.signals),0)
		errors  = np.append(errors,np.squeeze(group2.errors),0)
		return scangroup(energy,signals,errors,grouptype=gtype)
	else:
		energy  = np.append(energy,group2.energy[inds])
		signals = np.append(signals,np.squeeze(group2.signals[inds,:]),0)
		errors  = np.append(errors,np.squeeze(group2.errors[inds,:]),0)
		return scangroup(energy,signals,errors,grouptype=gtype)

def append2Scan_left(group1,group2,inds=None,grouptype='spectrum'):
	"""
	append two instancees of the scangroup class, return instance of scangroup
	append group1[inds] to the left (lower energies) of group2
	if inds is not None, only append rows indicated by inds to the first group 
	"""
	assert isinstance(group1,scangroup) and isinstance(group2,scangroup)
	if not inds:
		energy  = group1.energy
		signals = group1.signals
		errors  = group1.errors
		gtype   = group1.grouptype
		energy  = np.append(energy,group2.energy)
		signals = np.append(signals,np.squeeze(group2.signals),0)
		errors  = np.append(errors,np.squeeze(group2.errors),0)
		return scangroup(energy,signals,errors,grouptype=gtype)
	else:
		energy  = group1.energy[inds]
		signals = group1.signals[inds,:]
		errors  = group1.errors[inds,:]
		gtype   = group1.grouptype
		energy  = np.append(energy,group2.energy)
		signals = np.append(signals,np.squeeze(group2.signals),0)
		errors  = np.append(errors,np.squeeze(group2.errors),0)
		return scangroup(energy,signals,errors,grouptype=gtype)

def insertScan(group1,group2,grouptype='spectrum'):
	"""
	inserts group2 into group1
	NOTE! there is a numpy insert function, maybe it would be better to use that one!
	"""
	# find indices for below and above group2
	lowinds  = np.where(group1.energy<group2.get_estart())
	highinds = np.where(group1.energy>group2.get_eend())

	energy  = np.append(group1.energy[lowinds],np.append(group2.energy,group1.energy[highinds]))
	signals = np.append(np.squeeze(group1.signals[lowinds,:]),np.append(group2.signals,np.squeeze(group1.signals[highinds,:]),0),0)
	errors  = np.append(np.squeeze(group1.errors[lowinds,:]),np.append(group2.errors,np.squeeze(group1.errors[highinds,:]),0),0)

	return scangroup(energy,signals,errors,grouptype)

def catScansLong(groups,include_elastic):
	"""
	takes a longscan and inserts other backgroundscans (scans that have 'long' in their name) and other scans and inserts them into the long scan. 
	"""
	# the long scan
	spectrum = groups['long']

	# groups that don't have 'long' in the grouptype	
	allgroups  = []
	for group in groups:
		if not 'long' in group:
			allgroups.append(groups[group])
	allgroups.sort(key = lambda x:x.get_estart())

	# groups that have 'long' in the grouptype	
	longgroups = []
	for group in groups:
		if 'long' in group and group != 'long':
			longgroups.append(groups[group])
	longgroups.sort(key = lambda x:x.get_estart())

	# if there are other longscans: insert those first into the long scan
	for group in longgroups:
		spectrum = insertScan(spectrum,group)

	# insert other scans into the long scan
	for group in allgroups:
		spectrum = insertScan(spectrum,group)

	# cut off the elastic line if present in the groups
	if 'elastic' in groups and not include_elastic:
		inds = np.where(spectrum.energy > groups['elastic'].get_eend())[0]
		return spectrum.energy[inds], spectrum.signals[inds,:], spectrum.errors[inds,:]
	else:
		return spectrum.energy, spectrum.signals, spectrum.errors

def catScans(groups,include_elastic):
	"""
	concatenate all scans in groups, return the appended energy, signals, and errors
	"""
	# sort the groups by their start-energy
	allgroups  = []
	for group in groups:
		allgroups.append(groups[group])
	allgroups.sort(key = lambda x:x.get_estart())

	# assign first group to the spectrum (which is an instance of the scangroup class as well)
	spectrum = scangroup(allgroups[0].energy,allgroups[0].signals,allgroups[0].errors,grouptype='spectrum')

	# go through all other groups and append them to the right of the spectrum
	if len(allgroups)>1: # check if there are groups to append
		for group in allgroups[1:]:
			spectrum = append2Scan_right(spectrum,group)

	# cut off the elastic line if present in the groups
	if 'elastic' in groups and not include_elastic:
		inds = np.where(spectrum.energy > groups['elastic'].get_eend())[0]
		return spectrum.energy[inds], spectrum.signals[inds,:], spectrum.errors[inds,:]
	else:
		return spectrum.energy, spectrum.signals, spectrum.errors

def appendScans(groups,include_elastic):
    """
    try including different background scans... 
    append groups of scans ordered by their first energy value.
    long scans are inserted into gaps that at greater than two times the grid of the finer scans
    """
    # find all types of groups	
    grouptypes = [key for key in groups.keys()]

    if 'long' in grouptypes:
        print " going to refine "
        return catScansLong(groups,include_elastic)
    else: 
        return catScans(groups,include_elastic)

def catXESScans(groups):
	"""
	Concatenate all scans in groups, return the appended energy, signals, and errors.
	This needs to be a bit smarter to also work for scans that are scanned from small to large energy...
	"""
	# sort the groups by their end-energy (is the smallest in XES/energy2-scans)
	allgroups  = []
	for group in groups:
		if groups[group].energy[0] > groups[group].energy[-1]:
			groups[group].energy = np.flipud( groups[group].energy )
			for ii in range(groups[group].signals.shape[1]):
				groups[group].signals[:,ii] = np.flipud( groups[group].signals[:,ii] )
		allgroups.append(groups[group])
	allgroups.sort(key = lambda x:x.get_eend())

	# find lowest energy, highest energy, smallest increment, define grid
	eStart = np.amin([   np.amin(allgroups[ii].energy) for ii in range(len(allgroups))  ])
	eStop  = np.amax([   np.amax(allgroups[ii].energy) for ii in range(len(allgroups))  ])
	eStep  = np.amin(np.abs(  [np.diff(group.energy)[0] for group in allgroups]  ))

	energy  = np.arange(eStart,eStop,eStep)
	signals = np.zeros((len(energy),group.signals.shape[1],len(allgroups)))
	errors  = np.zeros((len(energy),group.signals.shape[1],len(allgroups)))

	# interpolate all groups onto grid, put into a matrix
	for group,ii in zip(allgroups,range(len(allgroups))):
		for jj in range(group.signals.shape[1]):
			interp_signals   = np.interp(energy, group.energy, group.signals[:,jj], left=0.0, right=0.0)
			signals[:,jj,ii] = interp_signals
			interp_errors    = np.interp(energy, group.energy, group.errors[:,jj], left=0.0, right=0.0)
			errors[:,jj,ii]  = interp_errors


	# sum up and weigh by number of available non-zero points
	sum_signals = np.zeros( (len(energy), group.signals.shape[1]))
	sum_errors  = np.zeros( (len(energy), group.signals.shape[1]))
	for jj in range(group.signals.shape[1]):
		for ii in range(len(energy)):
			nParts = len(np.where(signals[ii,jj]>0.0)[0])
			sum_signals[ii,jj] = np.sum(signals[ii,jj])/nParts
			sum_errors[ii,jj]  = np.sqrt(np.sum(errors[ii,jj]**2.0))/nParts

	spectrum = scangroup(energy,sum_signals,sum_errors,grouptype='spectrum')
	return spectrum.energy, spectrum.signals, spectrum.errors

def appendXESScans(groups):
	"""
	try including different background scans... 
	append groups of scans ordered by their first energy value.
	long scans are inserted into gaps that at greater than two times the grid of the finer scans
	"""
	# find all types of groups	
	grouptypes = [key for key in groups.keys()]
	return catXESScans(groups)

def create_sum_image(scans,scannumbers):
	"""
	Returns a summed image from all scans with numbers 'scannumbers'.
	scans       = dictionary of objects from the scan-class
	scannumbers = single scannumber, or list of scannumbers from which an image should be constructed
	"""
	# make 'scannumbers' iterable (even if it is just an integer)
	numbers = []
	if not isinstance(scannumbers,list):
		numbers.append(scannumbers)
	else:
		numbers = scannumbers

	key = 'Scan%03d' % numbers[0]
	image = np.zeros_like(scans[key].edfmats[0,:,:])
	for number in numbers:
		key = 'Scan%03d' % number
		for ii in range(scans[key].edfmats.shape[0]):
			image += scans[key].edfmats[ii,:,:]

	return image


def create_diff_image(scans,scannumbers,energy_keV):
	"""
	Returns a summed image from all scans with numbers 'scannumbers'.
	scans       = dictionary of objects from the scan-class
	scannumbers = single scannumber, or list of scannumbers from which an image should be constructed
	"""
	# make 'scannumbers' iterable (even if it is just an integer)
	numbers = []
	if not isinstance(scannumbers,list):
		numbers.append(scannumbers)
	else:
		numbers = scannumbers

	key = 'Scan%03d' % numbers[0]
	below_image = np.zeros_like(scans[key].edfmats[0,:,:])
	above_image = np.zeros_like(scans[key].edfmats[0,:,:])

	# find indices below and above 'energy'
	below_inds = scans[key].energy < energy_keV
	above_inds = scans[key].energy > energy_keV
	for number in numbers:
		key = 'Scan%03d' % number
		for ii in below_inds:
			below_image += scans[key].edfmats[ii,:,:]
		for ii in above_inds:
			above_image += scans[key].edfmats[ii,:,:]

	return (above_image - below_image)

def findRCscans(scans):
	""" **findRCscans**
	Returns a list of scans with name RC.
	"""
	RCscans = []
	for key in scans:
		if scans[key].get_type() == 'RC':
			RCscans.append(scans[key])
	return RCscans



