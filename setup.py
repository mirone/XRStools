from shutil import copyfile


import os, sys, glob,  platform, fnmatch


from distutils.core import setup
from distutils.core import  Extension, Command

import string
import stat


global version



version = [eval(l.split("=")[1]) for l in open(os.path.join(os.path.dirname(
    os.path.abspath(__file__)), "XRStools", "__init__.py"))
           if l.strip().startswith("version")][0]

print " VERSION " , version

try:
    import numpy
except ImportError:
    text = "You must have numpy installed.\n"
    raise ImportError, text

try:
    from Cython.Distutils import build_ext
    CYTHON = True
except ImportError:
    CYTHON = False

if CYTHON:
    cython_c_ext = ".pyx"
else:
    cython_c_ext = ".cpp"
    from distutils.command.build_ext import build_ext

data_files =  glob.glob("data/*.dat")+glob.glob("data/chitables/*.dat")+glob.glob("data/*.gif")+glob.glob("data/*.yaml")

from distutils.command.install_data import install_data

class smart_install_data_proper(install_data):
    def run(self):
        global  version
        install_cmd = self.get_finalized_command('install')
        self.install_dir = os.path.join(getattr(install_cmd, 'install_lib'),"XRStools"+version,"data")
        print "DATA to be installed in %s" %  self.install_dir

        pdir = os.path.join(getattr(install_cmd, 'install_lib'),"XRStools"+version)
        for filein in glob.glob('XRStools/*ui') :
            filedest = os.path.join(pdir, os.path.basename(filein))
            print "cp %s %s"%(filein,filedest)
            copyfile(filein, filedest)


 
        for filein in  glob.glob('XRStools/WIZARD/*ui'):
            filedest = os.path.join(pdir, "WIZARD",os.path.basename(filein))
            print "cp %s %s"%(filein,filedest)
            copyfile(filein, filedest)

        return install_data.run(self)

class smart_install_data(install_data):
    def run(self):
        global  version
        install_cmd = self.get_finalized_command('install')
        self.install_dir = os.path.join(getattr(install_cmd, 'install_lib'),"XRStools"+version,"..","..","..","..","share","XRStools","data")
        print "DATA to be installed in %s" %  self.install_dir

        install_lib_dir = os.path.join(getattr(install_cmd, 'install_lib'),"XRStools"+version )
        if not os.path.exists(self.install_dir):
            os.makedirs(self.install_dir)
        print " QUI " 
        for filein in glob.glob('XRStools/*ui'):
            filedest = os.path.join(install_lib_dir, os.path.basename(filein))
            print "cp %s %s"%(filein,filedest)
            copyfile(filein, filedest)


        return install_data.run(self)

###########################################################################################################
from distutils.command.install_scripts import install_scripts
class smart_install_scripts(install_scripts):
    def run (self):
        global  version
        
        install_cmd = self.get_finalized_command('install')
        self.install_dir = getattr(install_cmd, 'install_scripts')
        self.install_lib_dir = getattr(install_cmd, 'install_lib')

        # questo causava dei seri problemi
        # self.outfiles = self.copy_tree(self.build_dir, self.install_dir)
           
        self.outfiles = []
        for filein in glob.glob('XRStools/scripts/*'):
            if filein in glob.glob('XRStools/scripts/*~'): continue
            
            print "INSTALLO ", filein
            
            if not os.path.exists(self.install_dir):
                os.makedirs(self.install_dir)

            filedest = os.path.join(self.install_dir, os.path.basename(filein+version))
            if sys.platform =="win32":
                filedest=filedest+".py"

            print os.path.basename(filein+version)

            if os.path.exists(filedest):
                os.remove(filedest)
                
            text = open(filein, 'r').read()
            text=string.replace(text,"import XRStools", "import XRStools"+version)
            text=string.replace(text,"python", sys.executable)

            f=open(filedest, 'w')
            f.write(text)
            f.close()
            self.outfiles.append(filedest)

        if os.name == 'posix':
            # Set the executable bits (owner, group, and world) on
            # all the scripts we just installed.
            for file in self.get_outputs():
                if self.dry_run:
                    pass
                else:
                    mode = ((os.stat(file)[stat.ST_MODE]) | 0555) & 07777
                    os.chmod(file, mode)

if sys.platform == "win32":
    define_macros = [('WIN32',None)]
    script_files  = glob.glob("XRStools/scripts/*")
    script_files += glob.glob("scripts/*.bat")
    print "SCRIPTS " , script_files
else:
    script_files = glob.glob("XRStools/scripts/*")


# We subclass the build_ext class in order to handle compiler flags
# for openmp and opencl etc in a cross platform way
translator = {
        # Compiler
            # name, compileflag, linkflag
        'msvc' : {
            'openmp' : ('/openmp', ' '),
            'debug'  : ('/Zi', ' '),
            'OpenCL' : 'OpenCL',
            },
        'mingw32':{
            'openmp' : ('-fopenmp', '-fopenmp'),
            'debug'  : ('-g', '-g'),
            'stdc++' : 'stdc++',
            'OpenCL' : 'OpenCL'
            },
        'default':{
            'openmp' : ('-fopenmp', '-fopenmp'),
            'debug'  : ('-g', '-g'),
            'stdc++' : 'stdc++',
            'OpenCL' : 'OpenCL'
            }
        }

cmdclass = {}

class build_ext_tds2el(build_ext):
    ## this is for templating
    def build_extensions(self):

        if self.compiler.compiler_type in translator:
            trans = translator[self.compiler.compiler_type]
        else:            
            trans = translator['default']

 

        for e in self.extensions:
            e.extra_compile_args = [ trans[a][0] if a in trans else a
                                    for a in e.extra_compile_args]
            e.extra_link_args = [ trans[a][1] if a in trans else a
                                 for a in e.extra_link_args]

            if    e.libraries !=[ "fftw3f"] and e.libraries !=[ "mpi_cxx"]:
                e.libraries = filter(None, [ trans[a] if a in trans else None
                                             for a in e.libraries])



            # If you are confused look here:
            # print e, e.libraries
            # print e.extra_compile_args
            # print e.extra_link_args
        build_ext.build_extensions(self)

cmdclass['build_ext'] = build_ext_tds2el



class PyTest(Command):
    user_options = []
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        import sys, subprocess
        # os.chdir("test")
        # errno = subprocess.call([sys.executable, 'test_all.py'])
        # if errno != 0:
        #     raise SystemExit(errno)
        # else:
        #     os.chdir("..")
cmdclass['test'] = PyTest

#######################
# build_doc commandes #
#######################
sphinx = None
# try:
#     import sphinx
#     import sphinx.util.console
#     sphinx.util.console.color_terminal = lambda: False
#     from sphinx.setup_command import BuildDoc
# except ImportError:
#     sphinx = None

if sphinx:
    class build_doc(BuildDoc):

        def run(self):

            # make sure the python path is pointing to the newly built
            # code so that the documentation is built on this and not a
            # previously installed version

            build = self.get_finalized_command('build')
            sys.path.insert(0, os.path.abspath(build.build_lib))

            # Build the Users Guide in HTML and TeX format
            for builder in ('html', 'latex'):
                self.builder = builder
                self.builder_target_dir = os.path.join(self.build_dir, builder)
                self.mkpath(self.builder_target_dir)
                builder_index = 'index_{0}.txt'.format(builder)
                BuildDoc.run(self)
            sys.path.pop(0)
    cmdclass['build_doc'] = build_doc

ext_modules = []

#############################################################################

jn = os.sep.join

### TEMPLATE#################
c_sorgenti_tmp = ["spotpicker_cy"+cython_c_ext, "spotpicker.cc"]
c_sorgenti = [jn(['tds2el', 'tds2el_c', tok]) for tok in c_sorgenti_tmp]
depends_tmp = [ "spotpicker.h"]
depends = [jn(['tds2el', 'tds2el_c', tok]) for tok in depends_tmp]
cython_ext2 = Extension(name="spotpicker_cy",
                        sources=c_sorgenti,
                        depends=depends,
                        libraries=[ "mpi_cxx"],
                        include_dirs=[numpy.get_include()],
                        language="c++",             # generate C++ code
                        extra_compile_args=['-fopenmp'] ,
                        extra_link_args=['-fopenmp'] ,
                        )
## ext_modules.append(cython_ext2)
##########################################

c_sorgenti_tmp = ["luts_cy"+cython_c_ext, "luts.cc"]
c_sorgenti = [jn(['XRStools', 'XRStools_c', tok]) for tok in c_sorgenti_tmp]
depends_tmp = [ "luts.h"]
depends = [jn(['XRStools', 'XRStools_c', tok]) for tok in depends_tmp]
cython_ext2 = Extension(name="luts_cy",
                        sources=c_sorgenti,
                        depends=depends,
                        libraries=[ ],
                        # libraries=[ "mpi_cxx"],
                        include_dirs=[numpy.get_include()],
                        language="c++",             # generate C++ code
                        #extra_compile_args=['-fopenmp'] ,
                        #extra_link_args=['-fopenmp'] ,
                        )

ext_modules.append(cython_ext2)

##########################################

cmdclass['install_scripts'] = smart_install_scripts
cmdclass['install_data'] = smart_install_data_proper

# setup(name          = 'XRStools',
#       version       = version,
#       author        = "Christoph J. Sahle",
#       author_email  = "christoph.sahle@esrf.fr",
#       description   = 'An XRS data analysis package.',
#       url           = "christoph.sahle@esrf.fr",
#       download_url  = "",
#       ext_package   = "XRStools"+version,
#       scripts       = script_files,
#       packages      = ["XRStools"+version],
#       package_dir   = {"XRStools"+version:"XRStools" },
#       package_data  = {"XRStools"+version: ['data/*.dat','data/chitables/*.dat','data/refl_database/*.dat','docs/*.txt','examples/*.*']},
#       test_suite    = "test",
#       cmdclass      = cmdclass,
#       ext_modules   = ext_modules
# )



packages=["XRStools"+version, "XRStools"+version+".WIZARD"]
package_dir={"XRStools"+version:"XRStools" , "XRStools"+version+".WIZARD":"XRStools/WIZARD"}

for dirpath, dirnames, files in os.walk("XRStools/WIZARD/methods"):
    dirpathold=dirpath
    dirpath=dirpath.replace("/",".")
    dirpath=dirpath.replace("\\",".")
    packages.append(  "XRStools"+version+dirpath[len("XRStools"):]   )
    package_dir[packages[-1]] = dirpathold

    
setup(name='XRStools'+version,
      # version=version,
      author="Christoph Sahal",
      author_email="",
      description='descrizione da farsi qui',
      url="christoph.sahle@esrf.fr",
      download_url="",
      ext_package="XRStools"+version,
      scripts=script_files,
      # ext_modules=[Extension(**dico) for dico in ext_modules],
      packages=packages,
      package_dir=package_dir,
      test_suite="test",
      cmdclass=cmdclass,
      data_files=data_files,
      ext_modules=ext_modules
      )

# try:
#     import pyopencl
# except ImportError:
#     print("""sprsaocl can use pyopencl to run on parallel accelerators like GPU; this is an optional dependency.
# This python module can be found on:
# http://pypi.python.org/pypi/pyopencl
# """)





